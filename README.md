# DailyFiveApp

## Table of contents

* [Demo](#demo)
* [cordova update](#cordova-update)
* [JSDOC](#jsdoc)
* [Start node.js server](#start-nodejs-server)
* [Licence](#licence)

## Demo

[webapp](https://webapp.daily-five.net/)

## cordova update

```
cordova platform update ios
```

## JSDOC

To generate javasrcipt documentation you have to run:
```
jsdoc -r -c conf.json -d docs -u tuts
```
**Note:** If not installed: [jsdoc](https://github.com/jsdoc3/jsdoc)


## Start node.js server

Start server:
```
http-server [path_to_project] -o
```
**Note:** If not installed: [http-server](https://github.com/indexzero/http-server)

## Licence

The DailyFive App is licensed under the MIT license.