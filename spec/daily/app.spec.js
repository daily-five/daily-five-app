describe("Daily5:", function() {
  var Daily = require('../../assets/js/Daily');
  var daily;
  var task;

  beforeEach(function() {
    daily = new Daily();
    task = {
      id:21,
      name:'test'
    };
  });

  it('es sollte möglich sein einen neuen Task anzulegen', function() {
    expect(daily.newTask()).not.toBe(true);
    expect(daily.newTask('string')).not.toBe(true);
    expect(daily.newTask(36)).not.toBe(true);
    expect(daily.newTask([2,3,4])).not.toBe(true);
    expect(daily.newTask(task)).toBeGreaterThan(0);
  });

  // it('es sollte möglich sein einen Task zu bearbeiten/speichern', function() {
  //   expect(daily.saveTask()).not.toBe(true);
  //   expect(daily.saveTask('string')).not.toBe(true);
  //   expect(daily.saveTask(36)).not.toBe(true);
  //   expect(daily.saveTask(task)).toBe(true);
  // });
});
