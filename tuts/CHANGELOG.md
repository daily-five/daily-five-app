#### v1.0.2 - 2016-03-11
  - change: SASS App Themes; Eigene colors Datei
            Statusbar bekommt accent Color

#### v1.0.1 - 2016-03-04
  - new: Versionsüberprüfung und Updatefunktion
    - Daily(): isAppOutdated()
    - Daily(): outdatedBy()
  - new: Überarbeitete Dokumentation
  - new: Auflistung der verwendeten Bibliotheken unter Info
  - new: Sprachelement in den App Einstellungen (Vorbereitung für Fremdsprachen)

  - change: Content-Farbe der Aufgaben aller Themen
