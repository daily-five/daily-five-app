/* jshint undef: false, unused: false */
/* globals Framework7 */
/* globals Dom7 */

/**
 * @file Config File
 * @description Alle Konfigurationen für die App.
 * Objekt-Instanzen des Framework7, Daily, LocationController und
 * TasksController
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/*
 * Views:
 *   view-main, view-settings, popup-view-task, view-todo-list
 *
 * Pages:
 *   index, add-task, add-location, settings, location, location-detail,
 *   page-todo-list, about, theme
 *
 * Popups:
 *   popup-task, popup-location
 */

/** @var {Object} - Globale App Einstellungen */
var app_settings = {
  modalTitle: 'DailyFive',
  debugMode: false
};

/**
 * Framework7 Instanz
 * @type {Framework7}
 * @instance
 */
var todoApp = new Framework7({
  animateNavBackIcon:true,
  modalTitle: app_settings.modalTitle,
  scrollTopOnStatusbarClick: true
});

/** @var {Object} - Framework7 selectors engine */
var $$ = Dom7;

/**
 * Daily Instanz
 * @type {Daily}
 * @instance
 */
var daily = new Daily();

// Initalisiert App
// Ladet Einstellungen, Aufgaben und Orte vom LocalStorage
daily.initApp();

/**
 * LocationsController Instanz
 * @type {LocationsController}
 * @instance
 */
var locCtrl = new LocationsController(daily.getLocations());

/**
 * TasksController Instanz
 * @type {TasksController}
 * @instance
 */
var tasksCtrl = new TasksController();

/**
 * Rendert alle Listen neu
 * @function refresh
 * @memberof Daily
 * @since 0.0.1
 */
daily.refresh = function() {
  if (app_settings.debugMode)
    console.log('daily.refresh()');

  // Orte Liste
  locCtrl.renderList();
  // Daily Liste
  tasksCtrl.renderDailyList();
  // Todo Liste
  tasksCtrl.renderTodoList();
};

// Alle Listen Rendern
daily.refresh();

/**
 * @var {Object} - Framework7 Kalender Einstellungen; Monats- und Tagesnamen
 * auf Deutsch
 */
var calendarDefaults = {
  monthNames: ['Jänner', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli',
    'August' , 'September' , 'Oktober', 'November', 'Dezember'],
  monthNamesShort: ['Jän', 'Febr', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug',
    'Sept', 'Okt', 'Nov', 'Dez'],
  dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag',
    'Freitag', 'Samstag'],
  dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
};
