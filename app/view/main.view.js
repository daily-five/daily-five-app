/**
 * @file Main View Controlls
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/** @var {Object} - 'index' Page Settings */
var page_index = {
  name: 'index'
};

/** @var {Object} - 'mainView' View Objekt */
var mainView = todoApp.addView('#view-main', {
    dynamicNavbar: true,
    domCache: true
});

/**
 * Initialisierung der 'index' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_index.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_index.name);

  //

}).trigger();

/**
 * Re-Initialisierung der 'index' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_index.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_index.name);

  //

});

/**
 * Eventlistener: Open 'popup-task' Popup
 * @since 0.0.1
 */
$('a.popup-task-btn').on('click', function() {
  tasksCtrl.editTask('', function() {
    todoApp.popup('.popup-task');
  });
});

/**
 * Eventlistener: Aktualisieren aller Listen
 * @since 0.0.1
 */
$('a.refresh-task-btn').on('click', function() {
  daily.refresh();
});

/**
 * Eventlistener: Löscht Task
 * @since 0.0.1
 */
$('.list-block.daily-task-list').on('deleted', '.swipeout', function () {
  tasksCtrl.delTask($(this).data('loc-id').toString());
  daily.refresh();
});

/**
 * Eventlistener: Fügt dem Refernzdatum einen oder meherere Tage hinzu
 * @since 0.0.1
 */
$('body').on('click', '.btn-add-day', function () {
  var elem = $(this).closest('.swipeout');
  todoApp.modal({
    title:  app_settings.modalTitle,
    text: 'Um wie viele Tage soll die Aufgabe verschoben werden?',
    verticalButtons: true,
    buttons: [
      {
        text: '1 Tag',
        onClick: function() {
          tasksCtrl.addDays(elem.data('loc-id').toString(), 1, function() {
            daily.refresh();
          });
        }
      },
      {
        text: '2 Tage',
        onClick: function() {
          tasksCtrl.addDays(elem.data('loc-id').toString(), 2, function() {
            daily.refresh();
          });
        }
      },
      {
        text: '3 Tage',
        onClick: function() {
          tasksCtrl.addDays(elem.data('loc-id').toString(), 2, function() {
            daily.refresh();
          });
        }
      },
      {
        text: 'Abbrechen'
      }
    ]
  });
});

/**
 * Eventlistener: Setzt Task Status auf erledigt
 * @since 0.0.1
 */
$('.list-block.daily-task-list').on('click', '.todo-checklist__checkbox button',
                                    function () {
  var elem = $(this).closest('.swipeout');
  tasksCtrl.toggleCheck(elem.data('loc-id').toString(), function(state) {
    if (state) {
      elem.addClass('todo-checklist--checked');
    } else {
      elem.removeClass('todo-checklist--checked');
    }

    // Animation starten
    elem.delay(100).animate({
      height: "0"
    }, 400, function() {
      daily.refresh();
    });
  });
});

/**
 * Eventlistener: Task bearbeiten
 * @since 0.0.1
 */
$('.list-block.daily-task-list').on('click', '.todo-checklist__content',
                                    function () {
  var elem = $(this).closest('.swipeout');
  tasksCtrl.editTask(elem.data('loc-id').toString(), function() {
    todoApp.popup('.popup-task');
  });
});
