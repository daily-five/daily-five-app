/**
 * @file Settings View Controlls
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/** @var {Object} - 'settings' Page Settings */
var page_settings = {
  name: 'settings'
};

/** @var {Object} - 'location' Page Settings */
var page_location = {
  name: 'location'
};

/** @var {Object} - 'location-detail' Page Settings */
var page_location_detail = {
  name: 'location-detail'
};

/** @var {Object} - 'theme' Page Settings */
var page_theme = {
  name: 'theme'
};

/** @var {Object} - 'settingsView' View Objekt */
var settingsView = todoApp.addView('#view-settings', {
    dynamicNavbar: true,
    domCache: true
});



//******************************************************************************
// SettingsPage
//

/**
 * Initialisierung der 'settings' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_settings.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_settings.name);
}).trigger();

/**
 * Re-Initialisierung der 'settings' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_settings.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_settings.name);
});

/**
 * Eventlistener: App zurücksetzen; Alle Daten löschen
 * @since 0.0.1
 */
$('#reset-app').on('click', function() {
  todoApp.modal({
    title:  app_settings.modalTitle,
    text: 'Möchten Sie wirklich alle Daten löschen und Einstellungen \
           zurücksetzen?',
    buttons: [
      {
        text: 'JA',
        onClick: function() {
          daily.resetApp();
          daily.refresh();
        }
      },
      {
        text: 'NEIN'
      }
    ]
  });
});



//******************************************************************************
// LocationsPage
//

/**
 * Initialisierung der 'location' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_location.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_location.name);

  //
  var locations_cb = $$('#locations_cb');

  // Lade Einstellungen für CheckBox
  locations_cb[0].checked = daily.store_.settings.location;

}).trigger();

/**
 * Re-Initialisierung der 'location' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_location.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_location.name);

  //

});

/**
 * Eventlistener: Enable/Disable Orte Funktion Bei ändern der Ortefunktion
 * Listen neu Rendern
 * @since 0.0.1
 */
$$('#locations_cb').on('change', function() {
  daily.store_.settings.location = $$(this)[0].checked;
  daily.saveSettings();
  daily.refresh();
});

/**
 * Eventlistener: Ort Löschen
 * @since 0.0.1
 */
$('.list-block.locations-list').on('deleted', '.swipeout', function () {
  locCtrl.delLocation($(this).data('loc-id'));
  daily.refresh();
});

/**
 * Eventlistener: Enable/Disable Orte Funktion
 * @since 0.0.1
 */
$('.list-block.locations-list').on('click', '.item-link', function() {
  if (locCtrl.setCurrentLocationId($(this).closest('.swipeout')
      .data('locId'))) {
    settingsView.router.load({pageName: 'location-detail'});
  }
});

/**
 * Eventlistener: Open Popup 'popup-location'
 * @since 0.0.1
 */
$('.popup-loc').on('click', function() {
  locCtrl.emptyForm();
  $('.popup.popup-location').find('.center').text('Neuer Ort');
  $('.popup.popup-location').find('a.button.button-fill').text('Hinzufügen');
  todoApp.popup('.popup-location');
});



//******************************************************************************
// LocationsDetailPage
//

// var current_location;

/**
 * Initialisierung der 'location-detail' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_location_detail.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_location_detail.name);

  //
  locCtrl.setDetailForm();

}).trigger();

/**
 * Re-Initialisierung der 'location-detail' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_location_detail.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_location_detail.name);

  //
  locCtrl.setDetailForm();

});

/**
 * Eventlistener: Open Popup 'popup-location'
 * @since 0.0.1
 */
$('.popup-loc-edit').on('click', function() {
  locCtrl.emptyForm();
  locCtrl.setFormData(daily.getLocationById(locCtrl.getCurrentLocationId()));
  $('.popup.popup-location').find('.center').text('Bearbeiten');
  $('.popup.popup-location').find('a.button.button-fill').text('Speichern');
  todoApp.popup('.popup-location');
});

/**
 * Eventlistener: Funktion zum Löschen eines Ortes
 * @since 0.0.1
 */
$('#view-settings').on('click', 'a.loc-delete', function() {
  locCtrl.delLocation($(this).data('loc-id'), function() {
    settingsView.router.back();
    daily.refresh();
  });
});



//******************************************************************************
// ThemePage
//

/**
 * Initialisierung der 'theme' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_theme.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_theme.name);

  //

}).trigger();

/**
 * Re-Initialisierung der 'theme' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_theme.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_theme.name);

  //

});

/**
 * Eventlistener: Theme Funktion
 * @since 0.0.1
 */
 $('#theme-list').on('click', 'li', function() {
   daily.setTheme($(this).find('input').val());
 });
