/**
 * @file Todo List View Controlls
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/** @var {Object} - 'page-todo-list' Page Settings */
var page_todo_list = {
  name: 'page-todo-list'
};

/** @var {Object} - 'view-todo-list' View Objekt */
var todoListView = todoApp.addView('#view-todo-list', {
    dynamicNavbar: true,
    domCache: true
});

/**
 * Initialisierung der 'page-todo-list' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_todo_list.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_todo_list.name);

  //

}).trigger();

/**
 * Re-Initialisierung der 'page-todo-list' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_todo_list.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_todo_list.name);

  //

});

/**
 * Eventlistener: Anzeige der Todo Listen
 * @since 0.0.1
 */
$('#view-todo-list').on('click', '.subnavbar a.button', function () {
  $(this).parent().find('a.button').removeClass('active');
  $(this).addClass('active');
  tasksCtrl.setCurrentTaskList($(this).data('list-mode'));
  daily.refresh();
});

/**
 * Eventlistener: Löscht Task
 * @since 0.0.1
 */
$('.list-block.todo-task-list').on('deleted', '.swipeout', function () {
  tasksCtrl.delTask($(this).data('loc-id').toString());
  daily.refresh();
});

/**
 * Eventlistener: Wechselt Task Status
 * @since 0.0.1
 */
$('.list-block.todo-task-list').on('click', '.todo-checklist__checkbox button',
                                   function () {
  var elem = $(this).closest('.swipeout');
  tasksCtrl.toggleCheck(elem.data('loc-id').toString(), function(state) {
    if (state) {
      elem.find('.todo-checklist').addClass('todo-checklist--checked');
    } else {
      elem.find('.todo-checklist').removeClass('todo-checklist--checked');
    }

    // Animation starten und
    elem.delay(100).animate({
      height: "0"
    }, 400, function() {
      daily.refresh();
    });
  });
});

/**
 * Eventlistener: Task bearbeiten
 * @since 0.0.1
 */
$('.list-block.todo-task-list').on('click', '.todo-checklist__content',
                                   function () {
  var elem = $(this).closest('.swipeout');
  tasksCtrl.editTask(elem.data('loc-id').toString(), function() {
    todoApp.popup('.popup-task');
  });
});
