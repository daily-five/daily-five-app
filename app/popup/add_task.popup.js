/**
 * @file Add Task Popup Controlls
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/** @var {Object} - 'add-task' Page Settings */
var page_add_task = {
  name: 'add-task'
};

/** @var {Object} - 'popup-view-task' View Objekt */
var taskPopupView = todoApp.addView('#popup-view-task', {
    dynamicNavbar: true,
    domCache: true
});

/** @var {Object} - Kalender Objekt */
var taskCalendar = todoApp.calendar({
    input: '#task-calendar',
    dateFormat: 'dd.mm.yyyy',
    toolbar: true,
    toolbarCloseText: 'Fertig',
    toolbarTemplate: ' \
    <div class="toolbar"> \
      <div class="toolbar-inner"> \
        {{monthPicker}} \
        <a href="#" class="close-picker-calendar">Fertig</a> \
      </div> \
    </div>  \
    ',
    minDate: moment().subtract(1, 'days'),
    monthNames: calendarDefaults.monthNames,
    monthNamesShort: calendarDefaults.monthNamesShort,
    dayNames: calendarDefaults.dayNames,
    dayNamesShort: calendarDefaults.dayNamesShort
});

/**
 * Initialisierung der 'add-task' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_add_task.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_add_task.name);

  //

}).trigger();

/**
 * Re-Initialisierung der 'add-task' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_add_task.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_add_task.name);

  //

});

/**
 * Eventlistener: Priority Button Group Controlls
 * @since 0.0.1
 */
$('.popup.popup-task').on('click', '.buttons-row.task-priority > a',
                          function() {
  $('.buttons-row.task-priority > a').removeClass('active');
  $(this).addClass('active');
})

/**
 * Eventlistener: Date Mode Button Group Controlls
 * @since 0.0.1
 */
$('.popup.popup-task').on('click', '.buttons-row.task-date-mode > a',
                          function() {
  $('.buttons-row.task-date-mode > a').removeClass('active');
  $(this).addClass('active');
})

/**
 * Eventlistener: Save Task
 * @since 0.0.1
 */
$('.popup.popup-task').on('click', 'a.btn-task-save', function() {
  tasksCtrl.saveTask(function() {
    daily.refresh();
    todoApp.closeModal('.popup-task');
  });
})

/**
 * Eventlistener: Kalender Popup schließen
 * @since 0.0.1
 */
$('body').on('click', '.close-picker-calendar', function() {
  taskCalendar.close();
})
