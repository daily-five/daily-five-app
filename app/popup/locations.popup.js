/**
 * @file Locations Popup Controlls
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

/** @var {Object} - 'add-location' Page Settings */
var page_add_location = {
  name: 'add-location'
};

/**
 * Initialisierung der 'add-location' Page
 * @since 0.0.1
 */
todoApp.onPageInit(page_add_location.name, function (page) {
  if (app_settings.debugMode)
    console.log('Page initialized: '+page_add_location.name);

  //

}).trigger();

/**
 * Re-Initialisierung der 'add-location' Page
 * @since 0.0.1
 */
todoApp.onPageReinit(page_add_location.name, function (page) {
  if (app_settings.debugMode)
    console.info('Page re-initialized: '+page_add_location.name);

  //

});

/**
 * Eventlistener: Funktion zum Speichern eines Ortes
 * @since 0.0.1
 */
$('.popup.popup-location').on('click', 'a.loc-save', function() {
  locCtrl.saveLocation(function() {
    daily.refresh();
    locCtrl.setDetailForm();
    todoApp.closeModal('.popup-location');
  });
});
