// Install gulp
// npm install gulp del run-sequence gulp-manifest gulp-dev gulp-rev gulp-rename gulp-clean gulp-copy gulp-inject gulp-coffee gulp-concat gulp-file-include gulp-ftp gulp-imagemin jshint gulp-jshint gulp-plumber gulp-rename gulp-sass gulp-uglify gulp-csso gulp-uglifycss angular satellizer angular-ui-router ngstorage moment underscore jquery --save-dev

//------------------------------------------------------------------------------
// Initialisierung Projekt Module
//
var project_name = 'todo',

    // FTP Daten
    host = '',
    user = '',
    pass = '',
    remotePath = '';

var gulp = require('gulp'),
    del = require('del'),
    runSequence = require('run-sequence'),
    rev = require('gulp-rev'), // Hashen der Datei
    paths = require('./gulp7.config.json'),
    autoprefixer = require('gulp-autoprefixer'),
    gulpCopy = require('gulp-copy'),
    clean = require('gulp-clean'),
    manifest = require('gulp-manifest'),
    merge = require('merge-stream'),
    inject = require('gulp-inject'),
    dev = require('gulp-dev'),
    rename = require("gulp-rename"),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'), // Minifier JS
    htmlmin = require('gulp-htmlmin'),
    uglifycss = require('gulp-uglifycss'), // Minifier CSS
    csso = require('gulp-csso'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'), // SASS Compiler
    coffee = require('gulp-coffee'), // CoffeeScript Compiler
    plumber = require('gulp-plumber'), // Error handler
    imagemin = require('gulp-imagemin'), // Compress Image Files
    fileinclude = require('gulp-file-include'),
    pngquant = require('imagemin-pngquant'),
    ftp = require('gulp-ftp'); // FTP upload

//------------------------------------------------------------------------------
// JS Lint
//
gulp.task('lint', ['coffee'], function() {
  return gulp.src(paths.js)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

//------------------------------------------------------------------------------
// COPYFILES
//
gulp.task('copy:framework7css', function() {
  return gulp.src(paths.framework7css)
    .pipe(gulp.dest(paths.build + 'css'));
});
gulp.task('copy:index', function() {
  return gulp.src('./index_.html')
    .pipe(plumber())
    .pipe(rename('index.html'))
    .pipe(gulp.dest(paths.build));
});

//------------------------------------------------------------------------------
// CLEAN FILES
//
gulp.task('clean', function() {
  return del([
    './build/**/*'
  ]);
});

//------------------------------------------------------------------------------
// FILEINCLUDES
//
gulp.task('fileincludes', function() {
  return gulp.src(paths.app + 'index_.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(paths.build));
});

//------------------------------------------------------------------------------
// FILEINJECT
//
gulp.task('inject', ['fileincludes'], function () {
  var target = gulp.src('./build/index.html');
  var vendorcss = gulp.src(['./build/css/vendor*.min.css'], {read: false});
  var oscss = gulp.src(['./build/css/os*.min.css'], {read: false});
  var appcss = gulp.src(['./build/css/'+ project_name +'*.min.css'], {read: false});

  var vendorjs = gulp.src(['./build/js/vendor*.min.js'], {read: false});
  var appjs = gulp.src(['./build/js/'+ project_name +'*.min.js'], {read: false});
  //
  // var angular_third_modules = gulp.src(['./build/js/angular-app.min.js'], {read: false});
  //
  // var module = gulp.src('./app/**/*.module.js', {read: false});
  // var route = gulp.src('./app/**/*.route.js', {read: false});
  // var factory = gulp.src('./app/**/*.factory.js', {read: false});
  // var directive = gulp.src('./app/**/*.directive.js', {read: false});
  // var controller = gulp.src('./app/**/*.controller.js', {read: false});

  return target.pipe(inject(vendorcss,{starttag: '<!-- inject:vendorcss:{{ext}} -->', relative: true}))
    .pipe(inject(oscss,{starttag: '<!-- inject:oscss:{{ext}} -->', relative: true}))
    .pipe(inject(appcss,{starttag: '<!-- inject:appcss:{{ext}} -->', relative: true}))
    .pipe(inject(vendorjs,{starttag: '<!-- inject:vendorjs:{{ext}} -->', relative: true}))
    .pipe(inject(appjs,{starttag: '<!-- inject:appjs:{{ext}} -->', relative: true}))
    // .pipe(inject(angular_third_modules,{starttag: '<!-- inject:angthirdmod:{{ext}} -->', relative: true}))
    // .pipe(inject(module,{starttag: '<!-- inject:module:{{ext}} -->', relative: true}))
    // .pipe(inject(route,{starttag: '<!-- inject:route:{{ext}} -->', relative: true}))
    // .pipe(inject(factory,{starttag: '<!-- inject:factory:{{ext}} -->',relative: true}))
    // .pipe(inject(directive,{starttag: '<!-- inject:directive:{{ext}} -->',relative: true}))
    // .pipe(inject(controller,{starttag: '<!-- inject:controller:{{ext}} -->',relative: true}))
    .pipe(gulp.dest(paths.build));
});

//------------------------------------------------------------------------------
// COFFEE
// compile CoffeeScript, minify and combine
//
gulp.task('coffee', function () {
  return gulp.src('./assets/js/**/*.coffee')
    .pipe(coffee())
    .pipe(gulp.dest('./assets/js'));

});

//------------------------------------------------------------------------------
// JAVASCRIPT
// compile CoffeeScript, minify and combine
//
gulp.task('scripts', ['lint'], function () {
  return gulp
    .src([
      './assets/js/**/*.js',
      '!./assets/js/**/*.spec.js',
      './app/config.js',
      './app/view/*.js',
      './app/page/*.js',
      './app/popup/*.js'
    ])
    .pipe(plumber())
    .pipe(concat(project_name + '.js'))
    //.pipe(gulp.dest(paths.build + 'js'))
    .pipe(rename(project_name + '.min.js'))
    .pipe(uglify())
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'js'));
});

//------------------------------------------------------------------------------
// VendorJS
// compile CoffeeScript, minify and combine
//
gulp.task('vendorjs', function () {
  return gulp.src(paths.vendorjs)
    .pipe(plumber())
    .pipe(concat('vendor.min.js'))
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'js'));
});

//------------------------------------------------------------------------------
// Angular
// minify and combine angularfiles
//
gulp.task('angular', function () {
  return gulp.src(paths.angular_third_modules)
    .pipe(plumber())
    .pipe(concat('angular-app.min.js'))
    .pipe(gulp.dest(paths.build + 'js'));
});

//------------------------------------------------------------------------------
// CSS
// minify and combine
//
gulp.task('css', ['sass'], function () {
  return gulp.src(paths.css)
    .pipe(plumber())
    .pipe(autoprefixer({
			browsers: ['last 2 versions', 'iOS >= 6', 'Safari >= 6']
		}))
    .pipe(concat(project_name + '.min.css'))
    .pipe(csso())
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'css'));
});

//------------------------------------------------------------------------------
// VendorCSS
// minify and combine
//
gulp.task('vendorcss', function () {
  return gulp.src(paths.vendorcss)
    .pipe(plumber())
    .pipe(concat('vendor.min.css'))
    .pipe(csso())
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'css'));
});

//------------------------------------------------------------------------------
// OSCSS
// minify and combine
//
gulp.task('oscss:ios', function () {
  return gulp.src(paths.ioscss)
    .pipe(plumber())
    .pipe(concat('os.min.css'))
    .pipe(csso())
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'css'));
});
gulp.task('oscss:material', function () {
  return gulp.src(paths.materialcss)
    .pipe(plumber())
    .pipe(concat('os.min.css'))
    .pipe(csso())
    .pipe(rev())
    .pipe(gulp.dest(paths.build + 'css'));
});

//------------------------------------------------------------------------------
// SASS
// compile
//
gulp.task('sass', function () {
  return gulp.src(paths.sass)
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('assets/css'));
});

//------------------------------------------------------------------------------
// Fonts
// copy fonts
//
gulp.task('fonts', function () {
  return gulp.src(paths.fonts)
    .pipe(plumber())
    .pipe(gulp.dest(paths.build + 'fonts'));
});

//------------------------------------------------------------------------------
// Images
// copy images
//
gulp.task('images', function () {
  return gulp.src(paths.images)
    .pipe(plumber())
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.build + 'img'));
});

//------------------------------------------------------------------------------
// Maps
// copy javascript maps
//
gulp.task('maps', function () {
  return gulp.src(paths.maps)
    .pipe(plumber())
    .pipe(gulp.dest(paths.build + 'js'));
});

//------------------------------------------------------------------------------
// HTML minify
//
gulp.task('html-min', function () {
  return gulp.src(paths.build + 'index.html')
    .pipe(plumber())
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest(paths.build));
});

//------------------------------------------------------------------------------
// HTML manifest
//
gulp.task('manifest', function() {
  return gulp.src(paths.build + '**/*')
    .pipe(plumber())
    .pipe(manifest({
      hash: true,
      preferOnline: true,
      timestamp: true,
      //network: ['*'],
      filename: project_name + '.appcache',
      exclude: project_name + '.appcache'
     }))
    .pipe(gulp.dest('build'));
});

//------------------------------------------------------------------------------
// HTML Dev
//
gulp.task('dev:enable', function() {
  return gulp.src(paths.build + 'index.html')
    .pipe(plumber())
    .pipe(dev(false))
    .pipe(gulp.dest(paths.build));
});
gulp.task('dev:disable', function() {
  return gulp.src(paths.build + 'index.html')
    .pipe(plumber())
    .pipe(dev(true))
    .pipe(gulp.dest(paths.build));
});

//------------------------------------------------------------------------------
// Build Developement
// compile and copy files
//
gulp.task('default-build', function (callback) {
  return runSequence(
    'scripts',
    'vendorjs',
    // 'angular',
    'css',
    'vendorcss',
    'fonts',
    'maps',
    'images',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('DEFAULT BUILD');
      }
      callback(error);
  });
});

gulp.task('build', function (callback) {
  return runSequence(
    'clean',
    'default-build',
    'oscss:ios',
    'inject',
    'dev:disable',
    'html-min',
    'manifest',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('IOS BUILD FINISHED SUCCESSFULLY');
      }
      callback(error);
  });
});

gulp.task('build:ios', function (callback) {
  return runSequence(
    'clean',
    'default-build',
    'oscss:ios',
    'inject',
    'dev:enable',
    'html-min',
    'manifest',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('IOS BUILD FINISHED SUCCESSFULLY');
      }
      callback(error);
  });
});

gulp.task('build:material', function (callback) {
  return runSequence(
    'clean',
    'default-build',
    'oscss:material',
    'inject',
    'dev:enable',
    'html-min',
    'manifest',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('MATERIAL BUILD FINISHED SUCCESSFULLY');
      }
      callback(error);
  });
});

//------------------------------------------------------------------------------
// WATCH
//
gulp.task('watch', function () {
  gulp.watch(paths.js, ['lint', 'scripts']);
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.css, ['css']);
});

//------------------------------------------------------------------------------
// Run all commands
//
gulp.task('default', ['build', 'watch']);
