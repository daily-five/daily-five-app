
/**
 * @file Daily5 Task Modul
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.2
 */

(function() {
  window.Daily = (function() {

    /**
     * @class
     * @global
     * @classdesc Daily Model;
     * @version 0.0.2
     */
    function Daily() {

      /**
       * Beeinhaltet alle Variablen zur Laufzeit
       * @name Daily#store_
       * @type {Object}
       */
      this.store_ = {

        /**
         * Beeinhaltet alle Einstellungen zur Laufzeit
         * @type {Object}
         * @alias store_.settings
         * @memberof! Daily#
         */
        settings: {},

        /**
         * Beeinhaltet alle Positionsdaten zur Laufzeit
         * @type {Object}
         * @alias store_.position
         * @memberof! Daily#
         */
        position: {
          locating: false,
          latitude: 0,
          longitude: 0,
          accuracy: 0,
          timestamp: 0
        },

        /**
         * Beeinhaltet alle Aufgaben zur Laufzeit
         * @type {Object}
         * @alias store_.tasks
         * @memberof! Daily#
         */
        tasks: [],

        /**
         * Beeinhaltet alle Orte zur Laufzeit
         * @type {Object}
         * @alias store_.locations
         * @memberof! Daily#
         */
        locations: []
      };

      /**
       * Standard App Einstellungen
       * @name Daily#settings_default_
       * @type {Object}
       */
      this.settings_default_ = {

        /**
         * Bestimmt ob die Ortefunktion verwendet wird oder nicht
         * @type {Boolean}
         * @alias settings_default_.location
         * @memberof! Daily#
         * @default false
         */
        location: false,

        /**
         * Bestimmt welches Theme in Verwendung ist
         * @type {String}
         * @alias settings_default_.theme
         * @memberof! Daily#
         * @default 'theme-daily'
         */
        theme: 'theme-daily',

        /**
         * Bestimmt die Versionsnummer der App
         * @type {String}
         * @alias settings_default_.version
         * @memberof! Daily#
         * @default '1.0.0'
         */
        version: '1.0.1',

        /**
         * App Standard Sprache
         * @type {String}
         * @alias settings_default_.lang
         * @memberof! Daily#
         * @default 'de'
         */
        lang: 'de'
      };

      /** @member {Object} - Einstellungsparameter zur Klasse */
      this.params_ = {
        prefix: {
          tasks: 'tasks',
          locations: 'locs',
          settings: 'settings'
        },
        offset: {
          max_offset: 64,
          time: Math.pow(2, 4),
          priority: Math.pow(2, 2),
          location: Math.pow(2, 1),
          datemode: Math.pow(2, 0)
        },
        position: {
          max_distance: 0.150,
          time_intervall: 5
        },
        timeout: 400,
        moment: {
          calendar: {
            sameDay: '[heute]',
            nextDay: '[morgen]',
            nextWeek: 'dddd',
            lastDay: '[gestern]',
            lastWeek: '[letzten] dddd',
            sameElse: 'DD.MM.YYYY'
          }
        }
      };
    }


    /**
     * Initialisiert die App
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.initApp = function() {
      this.initSettings();
      if (this.isAppOutdated()) {
        switch (this.outdatedBy()) {
          case 'major':
            break;
          case 'minor':
            break;
          case 'rev':
            console.info('update version form ' + this.store_.settings.version + ' to ' + this.settings_default_.version);
            this.store_.settings.version = this.settings_default_.version;
            this.saveSettings();
            break;
        }
      }
      this.initTasks();
      this.initLocations();
      this.setTheme(this.store_.settings.theme);
      $('#version').text(this.store_.settings.version);
      return true;
    };


    /**
     * Speichert Daten in localStorage
     * @memberof Daily
     * @since 0.0.1
     * @param {String} key - Bezeichnung des Schlüssels
     * @param {String} data - Wert der Daten
     * @returns {Boolean}
     */

    Daily.prototype.saveToStorage = function(key, data) {
      if (key && data) {
        localStorage.setItem(key, data);
        return true;
      } else {
        return false;
      }
    };


    /**
     * Ladet Daten vom localStorage
     * @memberof Daily
     * @since 0.0.1
     * @param {String} key - Bezeichnung des Schlüssels
     * @returns {Boolean|String} Gespeicherter Wert
     */

    Daily.prototype.loadFromStorage = function(key) {
      if (key) {
        return localStorage.getItem(key);
      } else {
        return false;
      }
    };


    /**
     * Ladet alle Daten vom localStorage
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array} Array mit allen im localStorage hinterlegten Objecten
     */

    Daily.prototype.loadAllFromStorage = function() {
      var elements, i, iter, obj, ref;
      elements = [];
      for (iter = i = 0, ref = localStorage.length - 1; i <= ref; iter = i += 1) {
        obj = {
          key: localStorage.key(iter),
          value: localStorage.getItem(localStorage.key(iter))
        };
        elements.push(obj);
      }
      return elements;
    };


    /**
     * Löscht einzelne Daten vom localStorage
     * @memberof Daily
     * @since 0.0.1
     * @param {String} key - Bezeichnung des Schlüssels
     * @returns {Boolean}
     */

    Daily.prototype.removeFromStorage = function(key) {
      if (key) {
        localStorage.removeItem(key);
        return true;
      } else {
        return false;
      }
    };


    /**
     * Löscht den gesamten localStorage
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.clearStorage = function(key) {
      localStorage.clear();
      return true;
    };


    /**
     * Überprüft ob die Positionserkennung vom Browser unterstützt wird
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.isGeolocationAvailable = function() {
      if (navigator.geolocation) {
        return true;
      } else {
        return false;
      }
    };


    /**
     * Ermittelt die aktuelle Position
     * @memberof Daily
     * @since 0.0.1
     * @param {Function} callback - Wird aufgerufen, wenn Position ermittelt wurde
     * @returns {Boolean}
     */

    Daily.prototype.getCurrentPosition = function(callback) {
      var error, options, success;
      if (typeof callback !== 'function') {
        callback = function() {};
      }
      if (!this.store_.settings.location) {
        callback();
        return;
      }
      if (moment(this.store_.position.timestamp).isAfter(moment().subtract(this.params_.position.time_intervall, 'minutes'))) {
        setTimeout(function() {
          callback();
        }, this.params_.timeout);
        return;
      }
      todoApp.showPreloader('Position bestimmen');
      success = (function(_this) {
        return function(position) {
          _this.store_.position.locating = true;
          _this.store_.position.latitude = position.coords.latitude;
          _this.store_.position.longitude = position.coords.longitude;
          _this.store_.position.accuracy = position.coords.accuracy;
          _this.store_.position.timestamp = position.timestamp;
          callback();
          todoApp.hidePreloader();
        };
      })(this);
      error = (function(_this) {
        return function(error) {
          $('#locations_cb').prop('checked', false);
          _this.store_.position.locating = false;
          _this.store_.settings.location = false;
          _this.saveSettings();
          callback();
          todoApp.hidePreloader();
          switch (error.code) {
            case 1:
              todoApp.alert('Ortung nicht möglich! Fehlende Berechtigung zum Bestimmen der Position!');
              break;
            case 2:
              todoApp.alert('Ortung nicht möglich! Fehlendes Signal zum Bestimmen der Position!');
              break;
            case 3:
              todoApp.alert('Ortung nicht möglich! Timeout!');
              break;
            default:
              todoApp.alert('Ortung nicht möglich! ', +error.message);
          }
        };
      })(this);
      options = {
        timeout: 15000,
        enableHighAccuracy: true
      };
      navigator.geolocation.getCurrentPosition(success, error, options);
    };


    /**
     * Ermittelt die Distanz zweier Positionen (in km)
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Object mit GeoDaten
     * @returns {Number} - Entfernung zweier Punkte in km
     */

    Daily.prototype.getGeoDistance = function(data) {
      var dx, dy, lat;
      if (!(data && typeof data === 'object')) {
        return 0;
      }
      lat = (data.lat1 + data.lat2) / 2 * 0.01745;
      dx = 111.3 * Math.cos(lat) * (data.long1 - data.long2);
      dy = 111.3 * (data.lat1 - data.lat2);
      return Math.sqrt(dx * dx + dy * dy);
    };


    /**
     * Überprüft ob Aktueller Standort in Nähe von gespeicherten Ort ist
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Task Objekt
     * @returns {Boolean}
     */

    Daily.prototype.isLocation = function(data) {
      var i, is_location, len, loc, pos, ref;
      if (!(data && typeof data === 'object')) {
        return false;
      }
      if (data.locations.length <= 0) {
        return false;
      }
      is_location = false;
      pos = {
        lat1: this.store_.position.latitude,
        long1: this.store_.position.longitude,
        lat2: 0,
        long2: 0
      };
      ref = data.locations;
      for (i = 0, len = ref.length; i < len; i++) {
        loc = ref[i];
        if ((data = this.getLocationById(loc))) {
          pos.lat2 = data.position.lat;
          pos.long2 = data.position.long;
          if (this.getGeoDistance(pos) <= this.params_.position.max_distance) {
            is_location = true;
          }
        }
      }
      return is_location;
    };


    /**
     * Ändert bzw. Speichert die Software-Einstellungen
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Settings Objekt
     * @returns {Boolean}
     */

    Daily.prototype.saveSettings = function(data) {
      if (data && typeof data === 'object') {
        if (this.saveToStorage(this.params_.prefix.settings, JSON.stringify(data))) {
          this.store_.settings = data;
        }
      } else {
        this.saveToStorage(this.params_.prefix.settings, JSON.stringify(this.store_.settings));
      }
      return true;
    };


    /**
     * Ladet die Softwareeinstellungen vom LocalStorage
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.initSettings = function() {
      var element;
      if (!(element = this.loadFromStorage(this.params_.prefix.settings))) {
        this.store_.settings = this.settings_default_;
        this.saveSettings();
        return false;
      }
      this.store_.settings = $.extend({}, this.settings_default_, JSON.parse(element));
      this.saveSettings();
      return true;
    };


    /**
     * Überprüft ob die Lokalen Daten der aktuellen App Version entsprechen
     * @memberof Daily
     * @since 0.0.2
     * @returns {Boolean}
     */

    Daily.prototype.isAppOutdated = function() {
      if (this.store_.settings.version !== this.settings_default_.version) {
        return true;
      }
      return false;
    };


    /**
     * Ermittelt von welchem Versionssprung migriert werden soll
     * @memberof Daily
     * @since 0.0.2
     * @returns {String}
     */

    Daily.prototype.outdatedBy = function() {
      var app_v, loc_v, regex;
      regex = /^([0-9]+).([0-9]+).([0-9]+)$/;
      if ((loc_v = regex.exec(this.store_.settings.version)) !== null) {
        if (loc_v.index === regex.lastIndex) {
          regex.lastIndex++;
        }
      }
      if ((app_v = regex.exec(this.settings_default_.version)) !== null) {
        if (app_v.index === regex.lastIndex) {
          regex.lastIndex++;
        }
      }
      if (app_v[1] > loc_v[1]) {
        return 'major';
      }
      if (app_v[2] > loc_v[2]) {
        return 'minor';
      }
      if (app_v[3] > loc_v[3]) {
        return 'rev';
      }
      return false;
    };


    /**
     * Aktiviert das gewünschte Theme
     * @memberof Daily
     * @since 0.0.1
     * @param {String} theme - App Thema
     * @returns {Boolean}
     */

    Daily.prototype.setTheme = function(theme) {
      var theme_;
      theme_ = theme || 'theme-daily';
      if ($('body').hasClass(theme_)) {
        return true;
      }
      $('body').removeClass();
      $('body').addClass(theme_);
      this.store_.settings.theme = theme_;
      this.saveSettings();
      $('#theme-list').find(':checked').prop('checked', false);
      $('#theme-list').find('input:radio[value=' + theme_ + ']').prop('checked', true);
      return true;
    };


    /**
     * Löscht den gesamten Local Storage und setzt die Einstellungen zurück
     * @memberof Daily
     * @since 0.0.1
     */

    Daily.prototype.resetApp = function() {
      this.clearStorage();
      this.initSettings();
      this.setTheme();
      this.store_.tasks = [];
      this.store_.locations = [];
      this.store_.position = {
        locating: false,
        latitude: 0,
        longitude: 0,
        accuracy: 0,
        timestamp: 0
      };
      $('#locations_cb').prop('checked', false);
    };


    /**
     * Legt einen neuen Task an
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - neues Task Object
     * @returns {Boolean|Number}
     */

    Daily.prototype.newTask = function(data) {
      if (data && typeof data === 'object') {
        if (this.saveToStorage(this.params_.prefix.tasks + '-' + data.id, JSON.stringify(data))) {
          return this.store_.tasks.push(data);
        }
      }
      return false;
    };


    /**
     * Ändert bzw. Speichert einen bestehenden Task
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Boolean}
     */

    Daily.prototype.saveTask = function(data) {
      var i, is_new, iter, ref;
      is_new = true;
      if (data && typeof data === 'object') {
        if (this.saveToStorage(this.params_.prefix.tasks + '-' + data.id, JSON.stringify(data))) {
          for (iter = i = 0, ref = this.store_.tasks.length - 1; i <= ref; iter = i += 1) {
            if (this.store_.tasks[iter].id === data.id) {
              this.store_.tasks[iter] = data;
              is_new = false;
              break;
            }
          }
          if (is_new) {
            this.store_.tasks.push(data);
          }
          return true;
        }
      }
      return false;
    };


    /**
     * Löscht einen bestehenden Task
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Boolean|Number}
     */

    Daily.prototype.delTask = function(id) {
      var i, iter, ref;
      if (id) {
        if (this.removeFromStorage(this.params_.prefix.tasks + '-' + id.toString())) {
          for (iter = i = 0, ref = this.store_.tasks.length - 1; i <= ref; iter = i += 1) {
            if (this.store_.tasks[iter].id === id.toString()) {
              this.store_.tasks.splice(iter, 1);
              break;
            }
          }
        }
        return true;
      }
      return false;
    };


    /**
     * Ladet alle Tasks vom LocalStorage
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.initTasks = function() {
      var element, elements, i, len;
      elements = this.loadAllFromStorage();
      this.store_.tasks = [];
      for (i = 0, len = elements.length; i < len; i++) {
        element = elements[i];
        if (element.key.substring(0, this.params_.prefix.tasks.length) === this.params_.prefix.tasks) {
          this.store_.tasks.push(JSON.parse(element.value));
        }
      }
      return true;
    };


    /**
     * Ladet ein Task Objekt anhand der Id
     * @memberof Daily
     * @since 0.0.1
     * @param {String} id - ID eines Ortobjekts
     * @returns {Boolean|Object} - Ort Objekt
     */

    Daily.prototype.getTaskById = function(id) {
      var i, len, obj, ref, task;
      if (!id) {
        return false;
      }
      obj = {};
      ref = this.store_.tasks;
      for (i = 0, len = ref.length; i < len; i++) {
        task = ref[i];
        if (task.id === id.toString()) {
          obj = task;
          break;
        }
      }
      if (Object.keys(obj).length === 0) {
        return false;
      }
      return obj;
    };


    /**
     * Gibt alle nicht erledigten Tasks zurück
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array}
     */

    Daily.prototype.getTasks = function() {
      var tasks;
      tasks = [];
      tasks = this.store_.tasks.filter(function(task) {
        if (task.check !== true) {
          return task;
        }
      });
      return tasks;
    };


    /**
     * Gibt alle erledigten Tasks zurück
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array}
     */

    Daily.prototype.getTasksDone = function() {
      var tasks;
      tasks = [];
      tasks = this.store_.tasks.filter(function(task) {
        if (task.check) {
          return task;
        }
      });
      return tasks;
    };


    /**
     * Gibt alle abgelaufenen Tasks zurück
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array}
     */

    Daily.prototype.getTasksOutdated = function() {
      var tasks;
      tasks = [];
      tasks = this.store_.tasks.filter(function(task) {
        if (task.check !== true && moment(task.dates.date_ref).isBefore(moment(), 'day')) {
          return task;
        }
      });
      return tasks;
    };


    /**
     * Gibt alle Tasks mit einem bestimmten Datum zurück
     * @memberof Daily
     * @since 0.0.1
     * @param {Date} date - Referenzdatum
     * @returns {Array}
     */

    Daily.prototype.getTasksByDate = function(date) {
      var date_, tasks;
      date_ = moment(date) || moment();
      tasks = [];
      tasks = this.store_.tasks.filter(function(task) {
        if (task.check === true && moment(task.dates.date_checked).isSame(date_, 'day')) {
          return task;
        }
      });
      return tasks;
    };


    /**
     * Erweitert die Referenzzeit um x stunden
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @param {Number} time - Zeit in Stunden
     * @returns {Boolean}
     */

    Daily.prototype.addTime = function(data, time, val) {
      if (!(data && typeof data === 'object')) {
        return false;
      }
      if (!(time && typeof time === 'number')) {
        return false;
      }
      if (!(val && typeof val === 'string')) {
        return false;
      }
      data.dates.date_ref = moment(data.dates.date_ref).add(time, val).valueOf();
      this.saveTask(data);
      return true;
    };


    /**
     * Legt einen neuen Ort an
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - neues Ort Object
     * @returns {Boolean|Number}
     */

    Daily.prototype.newLocation = function(data) {
      if (data && typeof data === 'object') {
        if (this.saveToStorage(this.params_.prefix.locations + '-' + data.id, JSON.stringify(data))) {
          return this.store_.locations.push(data);
        }
      }
      return false;
    };


    /**
     * Ändert bzw. Speichert einen bestehenden Ort
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Ort Object
     * @returns {Boolean}
     */

    Daily.prototype.saveLocation = function(data) {
      var i, is_new, iter, ref;
      is_new = true;
      if (data && typeof data === 'object') {
        if (this.saveToStorage(this.params_.prefix.locations + '-' + data.id, JSON.stringify(data))) {
          for (iter = i = 0, ref = this.store_.locations.length - 1; i <= ref; iter = i += 1) {
            if (this.store_.locations[iter].id === data.id) {
              this.store_.locations[iter] = data;
              is_new = false;
              break;
            }
          }
          if (is_new) {
            this.store_.locations.push(data);
          }
          return true;
        }
      }
      return false;
    };


    /**
     * Löscht einen bestehenden Ort
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Ort Object
     * @returns {Boolean|Number}
     */

    Daily.prototype.delLocation = function(id) {
      var i, iter, ref;
      if (id) {
        if (this.removeFromStorage(this.params_.prefix.locations + '-' + id)) {
          for (iter = i = 0, ref = this.store_.locations.length - 1; i <= ref; iter = i += 1) {
            if (this.store_.locations[iter].id === id.toString()) {
              this.store_.locations.splice(iter, 1);
              break;
            }
          }
        }
        return true;
      }
      return false;
    };


    /**
     * Ladet alle Orte vom LocalStorage
     * @memberof Daily
     * @since 0.0.1
     * @returns {Boolean}
     */

    Daily.prototype.initLocations = function() {
      var element, elements, i, len;
      elements = this.loadAllFromStorage();
      this.store_.locations = [];
      for (i = 0, len = elements.length; i < len; i++) {
        element = elements[i];
        if (element.key.substring(0, this.params_.prefix.locations.length) === this.params_.prefix.locations) {
          this.store_.locations.push(JSON.parse(element.value));
        }
      }
      return true;
    };


    /**
     * Ladet ein Ort Objekt anhand der Id
     * @memberof Daily
     * @since 0.0.1
     * @param {String} id - ID eines Ortobjekts
     * @returns {Boolean|Object} - Ort Objekt
     */

    Daily.prototype.getLocationById = function(id) {
      var i, len, location, obj, ref;
      if (!id) {
        return false;
      }
      obj = {};
      ref = this.store_.locations;
      for (i = 0, len = ref.length; i < len; i++) {
        location = ref[i];
        if (location.id === id.toString()) {
          obj = location;
          break;
        }
      }
      if (Object.keys(obj).length === 0) {
        return false;
      }
      return obj;
    };


    /**
     * Gibt alle Orte aus
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array}
     */

    Daily.prototype.getLocations = function() {
      return this.store_.locations;
    };


    /**
     * Ausgabe der 5 Aufgaben
     * @memberof Daily
     * @since 0.0.1
     * @returns {Array}
     */

    Daily.prototype.dailyTasks = function() {
      var algo, algo_list, daily5, daily_list, i, iter, j, k, l, len, len1, ref, ref1, task, tasks_not_done, val;
      algo_list = Array(this.params_.offset.max_offset);
      tasks_not_done = this.getTasks();
      for (i = 0, len = tasks_not_done.length; i < len; i++) {
        task = tasks_not_done[i];
        val = 0;
        val += this.getModeOffset(task);
        val += this.getLocationOffset(task);
        val += this.getPriorityOffset(task);
        val += this.getDateOffset(task);
        if (typeof algo_list[val] === 'undefined') {
          algo_list[val] = Array();
        }
        task.rank = this.getRank(val);
        algo_list[val].push(task);
      }
      daily_list = Array();
      for (iter = j = ref = algo_list.length - 1; j >= 0; iter = j += -1) {
        if (typeof algo_list[iter] !== 'undefined') {
          ref1 = algo_list[iter];
          for (k = 0, len1 = ref1.length; k < len1; k++) {
            algo = ref1[k];
            daily_list.push(algo);
          }
        }
      }
      daily5 = Array();
      for (iter = l = 0; l <= 4; iter = l += 1) {
        if (typeof daily_list[iter] !== 'undefined') {
          daily5.push(daily_list[iter]);
        }
      }
      daily5.sort(function(a, b) {
        if (a.rank > b.rank) {
          return 1;
        }
        if (a.rank < b.rank) {
          return -1;
        }
        return 0;
      });
      return daily5;
    };


    /**
     * Bestimmt die Wertigkeit der Aufgabe
     * @memberof Daily
     * @since 0.0.1
     * @param {Number} val - Wert für Reihenfolge
     * @returns {String} - Wertebereich von 'A' bis 'D'
     */

    Daily.prototype.getRank = function(val) {
      if (!(val && typeof val === 'number')) {
        return false;
      }
      if ((val >= 0 && val <= 7) || (val >= 16 && val <= 23)) {
        return 'D';
      }
      if ((val >= 8 && val <= 15) || (val >= 24 && val <= 31)) {
        return 'C';
      }
      if ((val >= 32 && val <= 39) || (val >= 48 && val <= 55)) {
        return 'B';
      }
      if ((val >= 40 && val <= 47) || (val >= 56 && val <= 63)) {
        return 'A';
      }
      return false;
    };


    /**
     * Bestimmt den Offset des Datum-Mode
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Number}
     */

    Daily.prototype.getModeOffset = function(data) {
      if (!(data && typeof data === 'object')) {
        return false;
      }
      if (data.dates.mode === 'until') {
        return 0;
      } else {
        return this.params_.offset.datemode;
      }
    };


    /**
     * Bestimmt den Offset der Positionsbestimmung
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Number}
     */

    Daily.prototype.getLocationOffset = function(data) {
      if (!(data && typeof data === 'object')) {
        return false;
      }
      if (!this.store_.settings.location) {
        return 0;
      }
      if (this.isLocation(data)) {
        return this.params_.offset.location;
      }
      return 0;
    };


    /**
     * Bestimmt den Offset der Prioritäten
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Number}
     */

    Daily.prototype.getPriorityOffset = function(data) {
      if (!(data && typeof data === 'object')) {
        return false;
      }
      switch (data.priority) {
        case 1:
          return 3 * this.params_.offset.priority;
        case 2:
          return 2 * this.params_.offset.priority;
        case 3:
          return 1 * this.params_.offset.priority;
        case 4:
          return 0;
        default:
          return 0;
      }
    };


    /**
     * Bestimmt den Offset der Zeitangaben
     * @memberof Daily
     * @since 0.0.1
     * @param {Object} data - Bestehendes Task Object
     * @returns {Number}
     */

    Daily.prototype.getDateOffset = function(data) {
      var date;
      if (!(data && typeof data === 'object')) {
        return false;
      }
      date = moment(data.dates.date_ref);
      if (date.isSame(moment(), 'day')) {
        return 3 * this.params_.offset.time;
      }
      if (date.isBetween(moment(), moment().add(4, 'days'), 'day')) {
        return 2 * this.params_.offset.time;
      }
      if (date.isBetween(moment().add(3, 'days'), moment().add(8, 'days'), 'day')) {
        return 1 * this.params_.offset.time;
      }
      if (date.isAfter(moment().add(1, 'weeks'), 'day')) {
        return 0;
      }
      return 0;
    };


    /**
     * Bestimmt anhand eines Zeitstempels den passenden Text für die Aufgaben
     * @memberof Daily
     * @since 0.0.1
     * @param {Number} timestamp - Referenz Timestamp
     * @returns {String}
     */

    Daily.prototype.getTopicTimeText = function(timestamp) {
      if (!(timestamp && typeof timestamp === 'number')) {
        return '';
      }
      return moment(timestamp).locale(this.store_.settings.lang).calendar(null, this.params_.moment.calendar);
    };

    return Daily;

  })();

}).call(this);
