
/**
 * @file LocationsController
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

(function() {
  window.LocationsController = (function() {

    /**
     * @class
     * @global
     * @classdesc
     * @version 0.0.1
     */
    function LocationsController(list) {

      /** @member {Array} - Beeinhaltet die gespeicherten Orts-Objekte */
      this.list_ = list || [];

      /** @member {String} - ID des aktuellen Orts-Objekt */
      this.current_location_id_ = '';

      /** @member {String} - Google Geocoding API Key */
      this.api_key_ = 'AIzaSyBs6Pk7aY-X9Rpzm3fQpY7iQABNNM0ThpY';
    }


    /**
     * Initialisiert den Controller
     * @memberof LocationsController
     * @since 0.0.1
     * @returns {Boolean}
     */

    LocationsController.prototype.initApp = function() {};


    /**
     * Erzeugt ein neues DOM-Element
     * @memberof LocationsController
     * @since 0.0.1
     * @param {String} data - Wert der Daten
     * @returns {Object} - DOM Element
     */

    LocationsController.prototype.createElement = function(data) {
      var dom_element;
      dom_element = $('<li class="swipeout location" data-loc-id="' + data.id + '"></li>');
      dom_element.append(' <div class="swipeout-content"> <a href="#" class="item-link"> <div class="item-content"> <div class="item-inner"> <div class="item-title">' + data.name + '</div> </div> </div> </a> </div> <div class="swipeout-actions-right"> <a href="#" class="swipeout-delete swipeout-overswipe">Löschen</a> </div> ');
      return dom_element;
    };


    /**
     * Erstellt die Neue Liste
     * @memberof LocationsController
     * @since 0.0.1
     */

    LocationsController.prototype.renderList = function() {
      var element, i, len, list_block, ref;
      this.update();
      list_block = $('.list-block.locations-list');
      list_block.html('');
      if (this.list_.length <= 0) {
        list_block.append(' <div class="content-block text-center"> <p>Noch keine Orte angelegt</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
        return;
      }
      list_block.append('<ul></ul>');
      ref = this.list_;
      for (i = 0, len = ref.length; i < len; i++) {
        element = ref[i];
        list_block.children().append(this.createElement(element));
      }
    };


    /**
     * Befüllt die Deteilübersicht
     * @memberof LocationsController
     * @since 0.0.1
     */

    LocationsController.prototype.setDetailForm = function() {
      var current_location;
      current_location = daily.getLocationById(this.getCurrentLocationId());
      $('.item-title.loc-name').text(current_location.name);
      $('.item-title.loc-street').text(current_location.street);
      $('.item-title.loc-zip').text(current_location.zip);
      $('.item-title.loc-city').text(current_location.city);
      $('.list-button.loc-delete').data({
        "loc-id": current_location.id
      });
    };


    /**
     * Updated die Liste
     * @memberof LocationsController
     * @since 0.0.1
     * @param {Object} list - Liste mit den Orten
     */

    LocationsController.prototype.update = function(list) {
      this.list_ = list || daily.getLocations();
    };


    /**
     * Leert das Formular
     * @memberof LocationsController
     * @since 0.0.1
     */

    LocationsController.prototype.emptyForm = function() {
      $('.popup.popup-location').find('input.loc-id').val('');
      $('.popup.popup-location').find('input.loc-name').val('');
      $('.popup.popup-location').find('input.loc-street').val('');
      $('.popup.popup-location').find('input.loc-zip').val('');
      $('.popup.popup-location').find('input.loc-city').val('');
      $('.popup.popup-location').find('input.loc-long').val('');
      return $('.popup.popup-location').find('input.loc-lat').val('');
    };


    /**
     * Speichert neuen Ort
     * @memberof LocationsController
     * @since 0.0.1
     * @param {Function} callback - Optional für Navigation innerhalb der App
     * @returns {Boolean}
     */

    LocationsController.prototype.saveLocation = function(callback) {
      var ajax, form_data;
      todoApp.showIndicator();
      form_data = this.getFormData();
      ajax = {
        method: "GET",
        url: "https://maps.googleapis.com/maps/api/geocode/json",
        data: {
          address: form_data.street + ',' + form_data.zip + ',' + form_data.city,
          key: this.api_key_
        }
      };
      $.ajax(ajax).done((function(_this) {
        return function(data) {
          console.log(data);
          if (data.status === 'OK') {
            form_data.position.long = data.results[0].geometry.location.lng;
            form_data.position.lat = data.results[0].geometry.location.lat;
          } else {
            todoApp.hideIndicator();
            todoApp.alert('Adresse wurde nicht gefunden.');
            return;
          }
          daily.saveLocation(form_data);
          if (typeof callback === 'function') {
            callback();
          }
          todoApp.hideIndicator();
          console.log(form_data);
        };
      })(this)).fail((function(_this) {
        return function() {
          console.log('ajax request fail');
          todoApp.hideIndicator();
          todoApp.alert('Es konnte keine Verbindung zum Internet hergestellt werden');
        };
      })(this));
    };


    /**
     * Löscht Ort
     * @memberof LocationsController
     * @since 0.0.1
     * @param {String} id - Location ID
     * @param {Function} callback - Optional für Navigation innerhalb der App
     */

    LocationsController.prototype.delLocation = function(id, callback) {
      var id_;
      id_ = id.toString() || '';
      if (!(id_ && typeof id_ === 'string')) {
        return;
      }
      if (daily.delLocation(id_)) {
        this.update();
      }
      if (typeof callback === 'function') {
        callback();
      }
    };


    /**
     * Ausgabe der ID des aktuellen Orts-Objekts
     * @memberof LocationsController
     * @since 0.0.1
     * @returns {String}
     */

    LocationsController.prototype.getCurrentLocationId = function() {
      return this.current_location_id_;
    };


    /**
     * Holt Daten vom Formular und gibt neues Ort Objekt aus
     * @memberof LocationsController
     * @since 0.0.1
     * @returns {Object}
     */

    LocationsController.prototype.getFormData = function() {
      var form;
      form = {};
      form.position = {};
      form.id = $('.popup.popup-location').find('input.loc-id').val() || new Date().getTime().toString(16);
      form.name = $('.popup.popup-location').find('input.loc-name').val();
      form.street = $('.popup.popup-location').find('input.loc-street').val();
      form.zip = $('.popup.popup-location').find('input.loc-zip').val();
      form.city = $('.popup.popup-location').find('input.loc-city').val();
      form.position.long = $('.popup.popup-location').find('input.loc-long').val();
      form.position.lat = $('.popup.popup-location').find('input.loc-lat').val();
      return form;
    };


    /**
     * Setzen der ID des aktuellen Orts-Objekts
     * @memberof LocationsController
     * @since 0.0.1
     * @param {String} id - ID des aktuellen Orts-Objekts
     * @returns {Boolean}
     */

    LocationsController.prototype.setCurrentLocationId = function(id) {
      if (!(id && typeof id === 'string')) {
        return false;
      }
      this.current_location_id_ = id;
      return true;
    };


    /**
     * Füllt Formular aus
     * @memberof LocationsController
     * @since 0.0.1
     * @param {Object} data - Bestehendes Ort-Objekt
     * @returns {Boolean}
     */

    LocationsController.prototype.setFormData = function(data) {
      this.emptyForm();
      if (!(data && typeof data === 'object')) {
        return false;
      }
      $('.popup.popup-location').find('input.loc-id').val(data.id);
      $('.popup.popup-location').find('input.loc-name').val(data.name);
      $('.popup.popup-location').find('input.loc-street').val(data.street);
      $('.popup.popup-location').find('input.loc-zip').val(data.zip);
      $('.popup.popup-location').find('input.loc-city').val(data.city);
      $('.popup.popup-location').find('input.loc-long').val(data.position.long);
      $('.popup.popup-location').find('input.loc-lat').val(data.position.lat);
      return true;
    };

    return LocationsController;

  })();

}).call(this);
