###*
 * @file LocationsController
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
###

class window.LocationsController

  ###*
   * @class
   * @global
   * @classdesc
   * @version 0.0.1
  ###
  constructor: (list)->
    ###* @member {Array} - Beeinhaltet die gespeicherten Orts-Objekte ###
    @list_ = list or []

    ###* @member {String} - ID des aktuellen Orts-Objekt ###
    @current_location_id_ = '';

    ###* @member {String} - Google Geocoding API Key ###
    @api_key_ = 'AIzaSyBs6Pk7aY-X9Rpzm3fQpY7iQABNNM0ThpY';

  ###*
   * Initialisiert den Controller
   * @memberof LocationsController
   * @since 0.0.1
   * @returns {Boolean}
  ###
  initApp: ()->


  ###*
   * Erzeugt ein neues DOM-Element
   * @memberof LocationsController
   * @since 0.0.1
   * @param {String} data - Wert der Daten
   * @returns {Object} - DOM Element
  ###
  createElement: (data)->
    dom_element = $('<li class="swipeout location" data-loc-id="'+data.id+'"></li>')

    dom_element.append(' \
      <div class="swipeout-content"> \
        <a href="#" class="item-link"> \
          <div class="item-content"> \
            <div class="item-inner"> \
              <div class="item-title">' + data.name + '</div> \
            </div> \
          </div> \
        </a> \
      </div> \
      <div class="swipeout-actions-right"> \
        <a href="#" class="swipeout-delete swipeout-overswipe">Löschen</a> \
      </div> \
    ')

    return dom_element

  ###*
   * Erstellt die Neue Liste
   * @memberof LocationsController
   * @since 0.0.1
  ###
  renderList: ()->
    @update()

    list_block = $('.list-block.locations-list')
    # Liste leeren
    list_block.html('')

    # Abbruch, wenn noch keine Elemente angelegt sind
    if @list_.length <= 0
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Noch keine Orte angelegt</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
        </div> \
      ');
      return

    list_block.append('<ul></ul>');
    for element in @list_
      list_block.children().append(@createElement(element))
    return

  ###*
   * Befüllt die Deteilübersicht
   * @memberof LocationsController
   * @since 0.0.1
  ###
  setDetailForm: ()->
    current_location = daily.getLocationById @getCurrentLocationId()
    $('.item-title.loc-name').text current_location.name
    $('.item-title.loc-street').text current_location.street
    $('.item-title.loc-zip').text current_location.zip
    $('.item-title.loc-city').text current_location.city
    $('.list-button.loc-delete').data {"loc-id": current_location.id}
    return

  ###*
   * Updated die Liste
   * @memberof LocationsController
   * @since 0.0.1
   * @param {Object} list - Liste mit den Orten
  ###
  update: (list)->
    @list_ = list or daily.getLocations()
    return

  ###*
   * Leert das Formular
   * @memberof LocationsController
   * @since 0.0.1
  ###
  emptyForm: ()->
    $('.popup.popup-location').find('input.loc-id').val('')
    $('.popup.popup-location').find('input.loc-name').val('')
    $('.popup.popup-location').find('input.loc-street').val('')
    $('.popup.popup-location').find('input.loc-zip').val('')
    $('.popup.popup-location').find('input.loc-city').val('')
    $('.popup.popup-location').find('input.loc-long').val('')
    $('.popup.popup-location').find('input.loc-lat').val('')

  ###*
   * Speichert neuen Ort
   * @memberof LocationsController
   * @since 0.0.1
   * @param {Function} callback - Optional für Navigation innerhalb der App
   * @returns {Boolean}
  ###
  saveLocation: (callback)->
    todoApp.showIndicator()

    form_data = @getFormData()

    ajax =
    method: "GET"
    url: "https://maps.googleapis.com/maps/api/geocode/json"
    data:
      address: form_data.street + ',' + form_data.zip + ',' + form_data.city
      key: @api_key_

    # Abfrage Google Geocoding API
    $.ajax ajax

    # Abfrage erfolgreich
    .done (data) =>
      console.log data

      if data.status is 'OK'
        form_data.position.long = data.results[0].geometry.location.lng
        form_data.position.lat = data.results[0].geometry.location.lat
      else
        todoApp.hideIndicator()
        todoApp.alert('Adresse wurde nicht gefunden.')
        return

      daily.saveLocation(form_data)

      if typeof callback is 'function'
        callback()

      todoApp.hideIndicator()
      console.log form_data
      return

    # Abfrage fehlgeschlagen
    .fail () =>
      console.log 'ajax request fail'

      todoApp.hideIndicator()
      todoApp.alert('Es konnte keine Verbindung zum Internet hergestellt werden')
      return

    return

  ###*
   * Löscht Ort
   * @memberof LocationsController
   * @since 0.0.1
   * @param {String} id - Location ID
   * @param {Function} callback - Optional für Navigation innerhalb der App
  ###
  delLocation: (id, callback)->
    id_ = id.toString() or ''
    if not (id_ and typeof id_ is 'string')
      return

    if daily.delLocation(id_)
      @update()

    if typeof callback is 'function'
      callback()

    return

#*******************************************************************************
# Getter
#*******************************************************************************

  ###*
   * Ausgabe der ID des aktuellen Orts-Objekts
   * @memberof LocationsController
   * @since 0.0.1
   * @returns {String}
  ###
  getCurrentLocationId: ()->
    return @current_location_id_

  ###*
   * Holt Daten vom Formular und gibt neues Ort Objekt aus
   * @memberof LocationsController
   * @since 0.0.1
   * @returns {Object}
  ###
  getFormData: ()->
    form = {}
    form.position = {}

    form.id = $('.popup.popup-location').find('input.loc-id').val() or new Date().getTime().toString(16)
    form.name = $('.popup.popup-location').find('input.loc-name').val()
    form.street = $('.popup.popup-location').find('input.loc-street').val()
    form.zip = $('.popup.popup-location').find('input.loc-zip').val()
    form.city = $('.popup.popup-location').find('input.loc-city').val()
    form.position.long = $('.popup.popup-location').find('input.loc-long').val()
    form.position.lat = $('.popup.popup-location').find('input.loc-lat').val()
    return form

#*******************************************************************************
# Setter
#*******************************************************************************

  ###*
   * Setzen der ID des aktuellen Orts-Objekts
   * @memberof LocationsController
   * @since 0.0.1
   * @param {String} id - ID des aktuellen Orts-Objekts
   * @returns {Boolean}
  ###
  setCurrentLocationId: (id)->
    if not (id and typeof id is 'string')
      return false

    @current_location_id_ = id
    return true

  ###*
   * Füllt Formular aus
   * @memberof LocationsController
   * @since 0.0.1
   * @param {Object} data - Bestehendes Ort-Objekt
   * @returns {Boolean}
  ###
  setFormData: (data)->
    @emptyForm()
    if not (data and typeof data is 'object')
      return false

    $('.popup.popup-location').find('input.loc-id').val(data.id)
    $('.popup.popup-location').find('input.loc-name').val(data.name)
    $('.popup.popup-location').find('input.loc-street').val(data.street)
    $('.popup.popup-location').find('input.loc-zip').val(data.zip)
    $('.popup.popup-location').find('input.loc-city').val(data.city)
    $('.popup.popup-location').find('input.loc-long').val(data.position.long)
    $('.popup.popup-location').find('input.loc-lat').val(data.position.lat)
    return true
