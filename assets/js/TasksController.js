
/**
 * @file TasksController
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
 */

(function() {
  window.TasksController = (function() {

    /**
     * @class TasksController
     * @global
     * @classdesc
     * @version 0.0.1
     */
    function TasksController() {

      /** @member {String} - ID des aktuellen Orts-Objekt */
      this.current_task_id_ = '';

      /** @member {String} - Google Geocoding API Key */
      this.api_key_ = 'AIzaSyBs6Pk7aY-X9Rpzm3fQpY7iQABNNM0ThpY';

      /** @member {String} - Gibt an welche Liste angezeigt werden soll */
      this.current_task_list_ = 'topic';
    }


    /**
     * Initialisiert den Controller
     * @memberof TasksController
     * @since 0.0.1
     * @returns {Boolean}
     */

    TasksController.prototype.initApp = function() {};


    /**
     * Übersetzt den Rang in ein anderes Symbol
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} data - Rang
     * @returns {String} - Rang Symbol
     */

    TasksController.prototype.parseRank = function(data) {
      switch (data) {
        case 'A':
          return '<i class="daily-icon daily-app-star-four"></i>';
        case 'B':
          return '<i class="daily-icon daily-app-star-three"></i>';
        case 'C':
          return '<i class="daily-icon daily-app-star-two"></i>';
        default:
          return '<i class="daily-icon daily-app-star"></i>';
      }
    };


    /**
     * Erzeugt ein neues DOM-Element
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} data - Wert der Daten
     * @returns {Object} - DOM Element
     */

    TasksController.prototype.createDailyElement = function(data) {
      var dom_element, dom_topic_time, rank;
      dom_element = $('<li class="swipeout task" data-loc-id="' + data.id + '"></li>');
      rank = this.parseRank(data.rank);
      dom_topic_time = '<p><small>' + daily.getTopicTimeText(data.dates.date_ref) + '</small></p>';
      dom_element.append(' <div class="swipeout-content"> <div class="item-content"> <div class="todo-checklist todo-checklist--state-' + data.rank + '"> <div class="todo-checklist__priority"> ' + rank + ' </div> <!-- // end: todo-checklist__ --> <div class="todo-checklist__content"> ' + data.name + ' ' + dom_topic_time + ' </div> <!-- // end: todo-checklist__content --> <div class="todo-checklist__checkbox"> <button> <i class="daily-icon daily-app-circle"></i> </button> </div> <!-- // end: todo-checklist__checkbox --> </div> <!-- // end: todo-checklist --> </div> </div> <div class="swipeout-actions-left"> <a href="#" class="bg-blue swipeout-close btn-add-day"><i class="daily-icon daily-app-clock-outline"></i></a> </div> <div class="swipeout-actions-right"> <a class="swipeout-delete swipeout-close" href="#">Löschen</a> </div> ');
      return dom_element;
    };


    /**
     * Erzeugt neue Daily Liste
     * @memberof TasksController
     * @since 0.0.1
     */

    TasksController.prototype.renderDailyList = function() {
      daily.getCurrentPosition((function(_this) {
        return function() {
          var daily_list, element, i, len, list_block, tasks_today;
          daily_list = daily.dailyTasks();
          list_block = $('.list-block.daily-task-list');
          list_block.html('');
          if (daily.store_.tasks.length <= 0) {
            list_block.append(' <div class="content-block text-center"> <p>Keine Aufgaben vorhanden!</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> <p>Erstelle eine neue Aufgabe mit <i class="daily-icon daily-app-add"></i></p> </div> ');
            return;
          }
          tasks_today = daily.getTasksByDate();
          if (daily_list.length <= 0 && tasks_today.length <= 0) {
            list_block.append(' <div class="content-block text-center"> <p>Keine Aufgaben für heute</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
            return;
          } else if (daily_list.length <= 0 && tasks_today.length > 0) {
            list_block.append(' <div class="content-block text-center"> <p>Super! Für heute alles erledigt!</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
            return;
          }
          list_block.append('<ul></ul>');
          for (i = 0, len = daily_list.length; i < len; i++) {
            element = daily_list[i];
            list_block.children().append(_this.createDailyElement(element));
          }
        };
      })(this));
    };


    /**
     * Erzeugt ein neues DOM-Element
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} data - Wert der Daten
     * @returns {Object} - DOM Element
     */

    TasksController.prototype.createTodoElement = function(data) {
      var dom_element, dom_element_check, dom_element_class, dom_element_swipe, dom_topic_time;
      dom_element = $('<li class="swipeout task" data-loc-id="' + data.id + '"></li>');
      dom_element_class = '';
      dom_element_swipe = '<div class="swipeout-actions-left"> <a href="#" class="bg-blue swipeout-close btn-add-day"><i class="daily-icon daily-app-clock-outline"></i></a> </div>';
      dom_element_check = '<i class="daily-icon daily-app-circle"></i>';
      dom_topic_time = '<p><small>' + daily.getTopicTimeText(data.dates.date_ref) + '</small></p>';
      if (data.check) {
        dom_element_class = 'todo-checklist--checked';
        dom_element_swipe = '';
        dom_topic_time = '';
      }
      dom_element.append(' <div class="swipeout-content"> <div class="item-content"> <div class="todo-checklist todo-checklist--list ' + dom_element_class + '"> <div class="todo-checklist__list"> <div class="todo-checklist__content"> ' + data.name + ' ' + dom_topic_time + ' </div> <!-- // end: todo-checklist__content --> <div class="todo-checklist__checkbox"> <button> ' + dom_element_check + ' </button> </div> <!-- // end: todo-checklist__checkbox --> </div> </div> <!-- // end: todo-checklist --> </div> </div> ' + dom_element_swipe + '<div class="swipeout-actions-right"> <a class="swipeout-delete swipeout-close" href="#">Löschen</a> </div> ');
      return dom_element;
    };


    /**
     * Erzeugt neue Todo Liste
     * @memberof TasksController
     * @since 0.0.1
     */

    TasksController.prototype.renderTodoList = function() {
      var daily_list, element, i, len, list_block, tasks_today;
      list_block = $('.list-block.todo-task-list');
      list_block.html('');
      if (daily.store_.tasks.length <= 0) {
        list_block.append(' <div class="content-block text-center"> <p>Keine Aufgaben vorhanden!</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> <p>Erstelle eine neue Aufgabe mit <i class="daily-icon daily-app-add"></i></p> </div> ');
        return;
      }
      switch (this.current_task_list_) {
        case 'done':
          daily_list = daily.getTasksDone();
          break;
        case 'outdated':
          daily_list = daily.getTasksOutdated();
          break;
        default:
          daily_list = daily.getTasks();
      }
      tasks_today = daily.getTasksByDate();
      if (daily_list.length <= 0 && tasks_today.length <= 0 && this.current_task_list_ === 'done') {
        list_block.append(' <div class="content-block text-center"> <p>Noch keine erledigten Aufgaben</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
        return;
      } else if (daily_list.length <= 0 && tasks_today.length <= 0 && this.current_task_list_ === 'outdated') {
        list_block.append(' <div class="content-block text-center"> <p>Super! Noch keine verpassten Aufgaben</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
        return;
      } else if (daily_list.length <= 0 && tasks_today.length <= 0) {
        list_block.append(' <div class="content-block text-center"> <p>Keine Aufgaben für heute</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
        return;
      } else if (daily_list.length <= 0 && tasks_today.length > 0) {
        list_block.append(' <div class="content-block text-center"> <p>Super! Für heute alles erledigt!</p> <img src="./img/app/daily-five-logo-outline-mono.svg" /> </div> ');
        return;
      }
      list_block.append('<ul></ul>');
      for (i = 0, len = daily_list.length; i < len; i++) {
        element = daily_list[i];
        list_block.children().append(this.createTodoElement(element));
      }
    };


    /**
     * Setzt Formular zurück
     * @memberof TasksController
     * @since 0.0.1
     */

    TasksController.prototype.formReset = function() {
      var i, len, loc, locs, task_loc;
      task_loc = $('.popup.popup-task').find('a.task-locations');
      task_loc.find('select').val(null);
      task_loc.find('select').html('');
      task_loc.find('.item-after').text('');
      task_loc.parent().addClass('disabled');
      taskCalendar.close();
      taskCalendar.setValue([moment()]);
      $('.popup.popup-task').find('input.task-id').val('');
      $('.popup.popup-task').find('input.task-name').val('');
      $('.popup.popup-task').find('.buttons-row.task-priority > a').removeClass('active');
      $('.popup.popup-task').find('.buttons-row.task-priority > a:first-child').addClass('active');
      $('.popup.popup-task').find('.buttons-row.task-date-mode > a').removeClass('active');
      $('.popup.popup-task').find('.buttons-row.task-date-mode > a:first-child').addClass('active');
      $('.popup.popup-task').find('input.task-date').val('');
      $('.popup.popup-task').find('textarea.task-note').val('');
      locs = daily.getLocations();
      if (locs.length > 0) {
        for (i = 0, len = locs.length; i < len; i++) {
          loc = locs[i];
          task_loc.find('select').append($('<option value="' + loc.id + '">' + loc.name + '</option>'));
        }
        task_loc.parent().removeClass('disabled');
      }
    };


    /**
     * Speichert neuen Task
     * @memberof TasksController
     * @since 0.0.1
     * @param {Function} callback - Optional für Navigation innerhalb der App
     * @returns {Boolean}
     */

    TasksController.prototype.saveTask = function(callback) {
      var form_data;
      todoApp.showIndicator();
      form_data = this.getFormData();
      if (form_data.name === '') {
        todoApp.hideIndicator();
        todoApp.alert('Aufgabe benennen!');
        return;
      }
      if (daily.saveTask(form_data)) {
        if (typeof callback === 'function') {
          callback();
        }
      }
      todoApp.hideIndicator();
    };


    /**
     * Löscht Task
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} id - Location ID
     * @param {Function} callback - Optional für Navigation innerhalb der App
     */

    TasksController.prototype.delTask = function(id, callback) {
      var id_;
      id_ = id.toString() || '';
      if (!(id_ && typeof id_ === 'string')) {
        return;
      }
      if (daily.delTask(id_)) {
        if (typeof callback === 'function') {
          callback();
        }
      }
    };


    /**
     * Lädt Task
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} id - Location ID
     * @param {Function} callback - Optional für Navigation innerhalb der App
     */

    TasksController.prototype.editTask = function(id, callback) {
      this.formReset();
      if (this.setFormData(daily.getTaskById(id))) {
        $('.popup.popup-task').find('.center').text('Bearbeiten');
        $('.popup.popup-task').find('a.button.button-fill').text('Speichern');
      } else {
        $('.popup.popup-task').find('.center').text('Neue Aufgabe');
        $('.popup.popup-task').find('a.button.button-fill.new').text('Hinzufügen');
      }
      if (typeof callback === 'function') {
        callback();
      }
    };


    /**
     * Ändert Task Status
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} id - Location ID
     * @param {Function} callback - Optional für Navigation innerhalb der App
     * @returns {Boolean}
     */

    TasksController.prototype.toggleCheck = function(id, callback) {
      var data;
      if (!(id && typeof id === 'string')) {
        return false;
      }
      data = daily.getTaskById(id);
      data.check = data.check ? false : true;
      if (data.check) {
        data.dates.date_checked = new Date().getTime();
      } else {
        data.dates.date_checked = 0;
      }
      if (daily.saveTask(data)) {
        if (typeof callback === 'function') {
          callback(data.check);
        }
      }
      return true;
    };


    /**
     * Fügt der Referenzzeit einen Tag hinzu
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} id - Location ID
     * @param {Number} days - Anzahl der Tage
     * @param {Function} callback - Optional für Navigation innerhalb der App
     * @returns {Boolean}
     */

    TasksController.prototype.addDays = function(id, days, callback) {
      var data;
      if (!(id && typeof id === 'string')) {
        return false;
      }
      if (!(days && typeof days === 'number')) {
        return false;
      }
      data = daily.getTaskById(id);
      if (daily.addTime(data, days, 'days')) {
        if (typeof callback === 'function') {
          callback();
        }
      }
      return true;
    };


    /**
     * Ausgabe der ID des aktuellen Task-Objekts
     * @memberof TasksController
     * @since 0.0.1
     * @returns {String}
     */

    TasksController.prototype.getCurrentTaskId = function() {
      return this.current_task_id_;
    };


    /**
     * Ausgabe der Liste welche angezeigt wird
     * @memberof TasksController
     * @since 0.0.1
     * @returns {String}
     */

    TasksController.prototype.getCurrentTaskList = function() {
      return this.current_task_list_;
    };


    /**
     * Holt Daten vom Formular und gibt neues Ort Objekt aus
     * @memberof TasksController
     * @since 0.0.1
     * @returns {Object}
     */

    TasksController.prototype.getFormData = function() {
      var form;
      form = {};
      form.dates = {};
      form.check = false;
      form.id = $('.popup.popup-task').find('input.task-id').val() || new Date().getTime().toString(16);
      form.name = $('.popup.popup-task').find('input.task-name').val().trim();
      form.priority = parseInt($('.popup.popup-task').find('.buttons-row.task-priority > a.active').text());
      form.note = $('.popup.popup-task').find('textarea.task-note').val();
      form.dates.mode = $('.popup.popup-task').find('.buttons-row.task-date-mode > a.active').data('date-mode');
      form.dates.date = parseInt($('.popup.popup-task').find('input.task-date').val()) || new Date().getTime();
      form.dates.date_ref = new Date(taskCalendar.value[0]).getTime();
      form.dates.date_checked = 0;
      form.locations = $('.popup.popup-task').find('a.task-locations').find('select').val() || [];
      return form;
    };


    /**
     * Setzen der ID des aktuellen Task-Objekts
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} id - ID des aktuellen Orts-Objekts
     * @returns {Boolean}
     */

    TasksController.prototype.setCurrentTaskId = function(id) {
      if (!(id && typeof id === 'string')) {
        return false;
      }
      this.current_task_id_ = id;
      return true;
    };


    /**
     * Setzen der anzuzeigenden Liste
     * @memberof TasksController
     * @since 0.0.1
     * @param {String} type - Liste welche angezeigt werden soll
     * @returns {Boolean}
     */

    TasksController.prototype.setCurrentTaskList = function(type) {
      if (!(type && typeof type === 'string')) {
        return false;
      }
      this.current_task_list_ = type;
      return true;
    };


    /**
     * Füllt Formular aus
     * @memberof TasksController
     * @since 0.0.1
     * @param {Object} data - Bestehendes Ort-Objekt
     * @returns {Boolean}
     */

    TasksController.prototype.setFormData = function(data) {
      var i, id, len, loc, ref, text;
      this.formReset();
      if (!(data && typeof data === 'object')) {
        return false;
      }
      $('.popup.popup-task').find('input.task-id').val(data.id);
      $('.popup.popup-task').find('input.task-name').val(data.name);
      $('.popup.popup-task').find('textarea.task-note').val(data.note);
      $('.popup.popup-task').find('input.task-date').val(data.dates.date);
      taskCalendar.setValue([data.dates.date_ref]);
      $('.popup.popup-task').find('.buttons-row.task-priority > a').removeClass('active');
      $('.popup.popup-task').find('.buttons-row.task-priority > a:contains(' + data.priority + ')').addClass('active');
      $('.popup.popup-task').find('.buttons-row.task-date-mode > a').removeClass('active');
      $('.popup.popup-task').find('.buttons-row.task-date-mode > a[data-date-mode="' + data.dates.mode + '"]').addClass('active');
      $('.popup.popup-task').find('a.task-locations').find('select').val(data.locations);
      text = '';
      loc = {};
      ref = data.locations;
      for (i = 0, len = ref.length; i < len; i++) {
        id = ref[i];
        loc = daily.getLocationById(id);
        text += ', ' + loc.name;
      }
      $('.popup.popup-task').find('a.task-locations').find('.item-after').text(text.substring(2));
      return true;
    };

    return TasksController;

  })();

}).call(this);
