###*
 * @file TasksController
 * @description
 * @author Stephan Sandriesser
 * @version 0.0.1
###

class window.TasksController

  ###*
   * @class TasksController
   * @global
   * @classdesc
   * @version 0.0.1
  ###
  constructor: ()->
    ###* @member {String} - ID des aktuellen Orts-Objekt ###
    @current_task_id_ = '';

    ###* @member {String} - Google Geocoding API Key ###
    @api_key_ = 'AIzaSyBs6Pk7aY-X9Rpzm3fQpY7iQABNNM0ThpY';

    ###* @member {String} - Gibt an welche Liste angezeigt werden soll ###
    @current_task_list_ = 'topic';

  ###*
   * Initialisiert den Controller
   * @memberof TasksController
   * @since 0.0.1
   * @returns {Boolean}
  ###
  initApp: ()->



#*******************************************************************************
# Daily List Controll
#*******************************************************************************

  ###*
   * Übersetzt den Rang in ein anderes Symbol
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} data - Rang
   * @returns {String} - Rang Symbol
  ###
  parseRank: (data)->
    switch data
      when 'A' then return '<i class="daily-icon daily-app-star-four"></i>'
      when 'B' then return '<i class="daily-icon daily-app-star-three"></i>'
      when 'C' then return '<i class="daily-icon daily-app-star-two"></i>'
      else return '<i class="daily-icon daily-app-star"></i>'

  ###*
   * Erzeugt ein neues DOM-Element
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} data - Wert der Daten
   * @returns {Object} - DOM Element
  ###
  createDailyElement: (data)->
    dom_element = $('<li class="swipeout task" data-loc-id="' + data.id + '"></li>')

    rank = @parseRank(data.rank)
    dom_topic_time = '<p><small>' + daily.getTopicTimeText(data.dates.date_ref) + '</small></p>'

    dom_element.append(' \
      <div class="swipeout-content"> \
        <div class="item-content"> \
          <div class="todo-checklist todo-checklist--state-' + data.rank + '"> \
            <div class="todo-checklist__priority"> \
              ' + rank + ' \
            </div> \
            <!-- // end: todo-checklist__ --> \
            <div class="todo-checklist__content"> \
              ' + data.name + ' \
              ' + dom_topic_time + ' \
            </div> \
            <!-- // end: todo-checklist__content --> \
            <div class="todo-checklist__checkbox"> \
              <button> \
                <i class="daily-icon daily-app-circle"></i> \
              </button> \
            </div> \
            <!-- // end: todo-checklist__checkbox --> \
          </div> \
          <!-- // end: todo-checklist --> \
        </div> \
      </div> \
      <div class="swipeout-actions-left"> \
        <a href="#" class="bg-blue swipeout-close btn-add-day"><i class="daily-icon daily-app-clock-outline"></i></a> \
      </div> \
      <div class="swipeout-actions-right"> \
        <a class="swipeout-delete swipeout-close" href="#">Löschen</a> \
      </div> \
    ')

    return dom_element

  ###*
   * Erzeugt neue Daily Liste
   * @memberof TasksController
   * @since 0.0.1
  ###
  renderDailyList: ()->
    daily.getCurrentPosition ()=>
      daily_list = daily.dailyTasks()
      list_block = $('.list-block.daily-task-list')
      # Liste leeren
      list_block.html('')

      # Abbruch, wenn noch keine Elemente in der Datenbank sind
      if daily.store_.tasks.length <= 0
        list_block.append(' \
          <div class="content-block text-center"> \
            <p>Keine Aufgaben vorhanden!</p> \
            <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
            <p>Erstelle eine neue Aufgabe mit <i class="daily-icon daily-app-add"></i></p> \
          </div> \
        ');
        return

      tasks_today = daily.getTasksByDate()
      # Abbruch, wenn noch keine Elemente angelegt sind
      if daily_list.length <= 0 and tasks_today.length <= 0
        list_block.append(' \
          <div class="content-block text-center"> \
            <p>Keine Aufgaben für heute</p> \
            <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
          </div> \
        ');
        return

      else if daily_list.length <= 0 and tasks_today.length > 0
        list_block.append(' \
          <div class="content-block text-center"> \
            <p>Super! Für heute alles erledigt!</p> \
            <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
          </div> \
        ');
        return

      list_block.append('<ul></ul>');
      for element in daily_list
        list_block.children().append(@createDailyElement(element))
      return

    return

#*******************************************************************************
# Todo List Controll
#*******************************************************************************

  ###*
   * Erzeugt ein neues DOM-Element
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} data - Wert der Daten
   * @returns {Object} - DOM Element
  ###
  createTodoElement: (data)->
    dom_element = $('<li class="swipeout task" data-loc-id="' + data.id + '"></li>')

    dom_element_class = ''
    dom_element_swipe = '
      <div class="swipeout-actions-left"> \
        <a href="#" class="bg-blue swipeout-close btn-add-day"><i class="daily-icon daily-app-clock-outline"></i></a> \
      </div>'
    dom_element_check = '<i class="daily-icon daily-app-circle"></i>'
    dom_topic_time = '<p><small>' + daily.getTopicTimeText(data.dates.date_ref) + '</small></p>'

    if data.check
      dom_element_class = 'todo-checklist--checked'
      dom_element_swipe = ''
      dom_topic_time = ''



    dom_element.append(' \
      <div class="swipeout-content"> \
        <div class="item-content"> \
          <div class="todo-checklist todo-checklist--list ' + dom_element_class + '"> \
            <div class="todo-checklist__list"> \
              <div class="todo-checklist__content"> \
                ' + data.name + ' \
                ' + dom_topic_time + ' \
              </div> \
              <!-- // end: todo-checklist__content --> \
              <div class="todo-checklist__checkbox"> \
                <button> \
                  ' + dom_element_check + ' \
                </button> \
              </div> \
              <!-- // end: todo-checklist__checkbox --> \
            </div> \
          </div> \
          <!-- // end: todo-checklist --> \
        </div> \
      </div> \
      ' + dom_element_swipe + '
      <div class="swipeout-actions-right"> \
        <a class="swipeout-delete swipeout-close" href="#">Löschen</a> \
      </div> \
    ')

    return dom_element

  ###*
   * Erzeugt neue Todo Liste
   * @memberof TasksController
   * @since 0.0.1
  ###
  renderTodoList: ()->

    list_block = $('.list-block.todo-task-list')
    # Liste leeren
    list_block.html('')

    # Abbruch, wenn noch keine Elemente in der Datenbank sind
    if daily.store_.tasks.length <= 0
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Keine Aufgaben vorhanden!</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
          <p>Erstelle eine neue Aufgabe mit <i class="daily-icon daily-app-add"></i></p> \
        </div> \
      ');
      return


    switch @current_task_list_
      when 'done' then daily_list = daily.getTasksDone()
      when 'outdated' then daily_list = daily.getTasksOutdated()
      else daily_list = daily.getTasks()


    tasks_today = daily.getTasksByDate()


    # Abbruch, wenn noch keine Elemente angelegt sind
    if daily_list.length <= 0 and tasks_today.length <= 0 and @current_task_list_ is 'done'
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Noch keine erledigten Aufgaben</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
        </div> \
      ');
      return

    else if daily_list.length <= 0 and tasks_today.length <= 0 and @current_task_list_ is 'outdated'
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Super! Noch keine verpassten Aufgaben</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
        </div> \
      ');
      return

    else if daily_list.length <= 0 and tasks_today.length <= 0
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Keine Aufgaben für heute</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
        </div> \
      ');
      return

    else if daily_list.length <= 0 and tasks_today.length > 0
      list_block.append(' \
        <div class="content-block text-center"> \
          <p>Super! Für heute alles erledigt!</p> \
          <img src="./img/app/daily-five-logo-outline-mono.svg" /> \
        </div> \
      ');
      return

    list_block.append('<ul></ul>');
    for element in daily_list
      list_block.children().append(@createTodoElement(element))
    return

#*******************************************************************************
# Form Controll
#*******************************************************************************

  ###*
   * Setzt Formular zurück
   * @memberof TasksController
   * @since 0.0.1
  ###
  formReset: ()->
    # Orte
    task_loc = $('.popup.popup-task').find('a.task-locations')
    task_loc.find('select').val(null)
    task_loc.find('select').html('')
    task_loc.find('.item-after').text('')
    task_loc.parent().addClass('disabled')

    # Kalender
    taskCalendar.close()
    taskCalendar.setValue [moment()]

    # ID
    $('.popup.popup-task').find('input.task-id').val('')

    # Name
    $('.popup.popup-task').find('input.task-name').val('')

    # Priorität
    $('.popup.popup-task').find('.buttons-row.task-priority > a').removeClass('active')
    $('.popup.popup-task').find('.buttons-row.task-priority > a:first-child').addClass('active')

    # Datum Modus
    $('.popup.popup-task').find('.buttons-row.task-date-mode > a').removeClass('active')
    $('.popup.popup-task').find('.buttons-row.task-date-mode > a:first-child').addClass('active')

    # Erstellungs-Datum
    $('.popup.popup-task').find('input.task-date').val('')

    # Textarea
    $('.popup.popup-task').find('textarea.task-note').val('')

    locs = daily.getLocations()
    if locs.length > 0
      for loc in locs
        task_loc
          .find('select')
          .append($('<option value="'+loc.id+'">'+loc.name+'</option>'))

      task_loc.parent().removeClass('disabled')

    return

  ###*
   * Speichert neuen Task
   * @memberof TasksController
   * @since 0.0.1
   * @param {Function} callback - Optional für Navigation innerhalb der App
   * @returns {Boolean}
  ###
  saveTask: (callback)->
    todoApp.showIndicator()

    form_data = @getFormData()

    if form_data.name is ''
      todoApp.hideIndicator()
      todoApp.alert('Aufgabe benennen!');
      return

    if daily.saveTask(form_data)
      if typeof callback is 'function'
        callback()

    todoApp.hideIndicator()
    return

  ###*
   * Löscht Task
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} id - Location ID
   * @param {Function} callback - Optional für Navigation innerhalb der App
  ###
  delTask: (id, callback)->
    id_ = id.toString() or ''
    if not (id_ and typeof id_ is 'string')
      return

    if daily.delTask(id_)
      if typeof callback is 'function'
        callback()

    return

  ###*
   * Lädt Task
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} id - Location ID
   * @param {Function} callback - Optional für Navigation innerhalb der App
  ###
  editTask: (id, callback)->
    @formReset()

    # Fülle Form, wenn Task vorhanden
    if @setFormData daily.getTaskById(id)
      $('.popup.popup-task').find('.center').text('Bearbeiten');
      $('.popup.popup-task').find('a.button.button-fill').text('Speichern');
    else
      $('.popup.popup-task').find('.center').text('Neue Aufgabe');
      $('.popup.popup-task').find('a.button.button-fill.new').text('Hinzufügen');


    if typeof callback is 'function'
      callback()

    return

#*******************************************************************************
# Features
#*******************************************************************************

  ###*
   * Ändert Task Status
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} id - Location ID
   * @param {Function} callback - Optional für Navigation innerhalb der App
   * @returns {Boolean}
  ###
  toggleCheck: (id, callback)->
    if not (id and typeof id is 'string')
      return false

    data = daily.getTaskById(id)

    data.check = if data.check then false else true

    if data.check
      data.dates.date_checked = new Date().getTime()
    else
      data.dates.date_checked = 0

    if daily.saveTask(data)
      if typeof callback is 'function'
        callback data.check

    return true

  ###*
   * Fügt der Referenzzeit einen Tag hinzu
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} id - Location ID
   * @param {Number} days - Anzahl der Tage
   * @param {Function} callback - Optional für Navigation innerhalb der App
   * @returns {Boolean}
  ###
  addDays: (id, days, callback)->
    if not (id and typeof id is 'string')
      return false

    if not (days and typeof days is 'number')
      return false

    data = daily.getTaskById(id)

    if daily.addTime data, days, 'days'
      if typeof callback is 'function'
        callback()

    return true


#*******************************************************************************
# Getter
#*******************************************************************************

  ###*
   * Ausgabe der ID des aktuellen Task-Objekts
   * @memberof TasksController
   * @since 0.0.1
   * @returns {String}
  ###
  getCurrentTaskId: ()->
    return @current_task_id_

  ###*
   * Ausgabe der Liste welche angezeigt wird
   * @memberof TasksController
   * @since 0.0.1
   * @returns {String}
  ###
  getCurrentTaskList: ()->
    return @current_task_list_

  ###*
   * Holt Daten vom Formular und gibt neues Ort Objekt aus
   * @memberof TasksController
   * @since 0.0.1
   * @returns {Object}
  ###
  getFormData: ()->
    form = {}
    form.dates = {}
    form.check = false

    form.id = $('.popup.popup-task').find('input.task-id').val() or new Date().getTime().toString(16)
    form.name = $('.popup.popup-task').find('input.task-name').val().trim()
    form.priority = parseInt $('.popup.popup-task').find('.buttons-row.task-priority > a.active').text()
    form.note = $('.popup.popup-task').find('textarea.task-note').val()

    form.dates.mode = $('.popup.popup-task').find('.buttons-row.task-date-mode > a.active').data('date-mode')
    form.dates.date = parseInt($('.popup.popup-task').find('input.task-date').val()) or new Date().getTime()
    form.dates.date_ref = new Date(taskCalendar.value[0]).getTime()
    form.dates.date_checked = 0

    form.locations = $('.popup.popup-task').find('a.task-locations').find('select').val() or []

    return form

#*******************************************************************************
# Setter
#*******************************************************************************

  ###*
   * Setzen der ID des aktuellen Task-Objekts
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} id - ID des aktuellen Orts-Objekts
   * @returns {Boolean}
  ###
  setCurrentTaskId: (id)->
    if not (id and typeof id is 'string')
      return false

    @current_task_id_ = id
    return true

  ###*
   * Setzen der anzuzeigenden Liste
   * @memberof TasksController
   * @since 0.0.1
   * @param {String} type - Liste welche angezeigt werden soll
   * @returns {Boolean}
  ###
  setCurrentTaskList: (type)->
    if not (type and typeof type is 'string')
      return false

    @current_task_list_ = type
    return true

  ###*
   * Füllt Formular aus
   * @memberof TasksController
   * @since 0.0.1
   * @param {Object} data - Bestehendes Ort-Objekt
   * @returns {Boolean}
  ###
  setFormData: (data)->
    @formReset()
    if not (data and typeof data is 'object')
      return false

    $('.popup.popup-task').find('input.task-id').val(data.id)
    $('.popup.popup-task').find('input.task-name').val(data.name)
    $('.popup.popup-task').find('textarea.task-note').val(data.note)
    $('.popup.popup-task').find('input.task-date').val(data.dates.date)
    taskCalendar.setValue([data.dates.date_ref])
    $('.popup.popup-task').find('.buttons-row.task-priority > a').removeClass('active')
    $('.popup.popup-task').find('.buttons-row.task-priority > a:contains(' + data.priority + ')').addClass('active')
    $('.popup.popup-task').find('.buttons-row.task-date-mode > a').removeClass('active')
    $('.popup.popup-task').find('.buttons-row.task-date-mode > a[data-date-mode="' + data.dates.mode + '"]').addClass('active')

    $('.popup.popup-task').find('a.task-locations').find('select').val(data.locations)

    text = ''
    loc = {}
    for id in data.locations
      loc = daily.getLocationById id
      text += ', ' + loc.name

    $('.popup.popup-task').find('a.task-locations').find('.item-after').text text.substring 2

    return true
